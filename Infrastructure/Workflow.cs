using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using CommandLine;
using dotnetcore.Infrastructure.HeavyOperations;
using dotnetcore.Models;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace dotnetcore.Infrastructure;

public class Workflow : BackgroundService
{
    private readonly Settings _settings;
    private readonly ILogger _logger;
    private readonly CreateRandomData _createRandomData;
    private readonly WithThread _withThread;
    private readonly WithThreadPool _withThreadPool;
    private readonly WithTask _withTask;
    private readonly IHostApplicationLifetime _hostApplicationLifetime;

    public Workflow(IOptions<Settings> options, ILogger<Workflow> logger, CreateRandomData createRandomData, 
        WithTask withTask, WithThread withThread, WithThreadPool withThreadPool, IHostApplicationLifetime hostApplicationLifetime)
    {
        _settings = options.Value;
        _logger = logger;
        _createRandomData = createRandomData;
        _withTask = withTask;
        _withThread = withThread;
        _withThreadPool = withThreadPool;
        _hostApplicationLifetime = hostApplicationLifetime;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("dotnet.homework.23 async file load");

        // Запуск генерации файла 
        var commandLineOptions = Parser.Default.ParseArguments<CommandLineOptions>(Environment.GetCommandLineArgs()).Value;
        if (string.IsNullOrEmpty(commandLineOptions.FileName) == false)
        {
            try
            {
                if (File.Exists(commandLineOptions.FileName) == false)
                {
                    File.Create(commandLineOptions.FileName);
                    _logger.LogInformation($"file {Path.GetFileName(commandLineOptions.FileName)} was created");
                }
            }
            catch (Exception e)
            {
                _logger.LogWarning($"error when creating file, message: {e.Message}");
                _hostApplicationLifetime.StopApplication();
                return;
            } 
            
            await _createRandomData.CreateFileWithRandomData(commandLineOptions.FileName, commandLineOptions.NumberOfRecords, stoppingToken);
            _hostApplicationLifetime.StopApplication();
            return;
        }

        // Создаем файлы для проверки
        var filePaths = new[]
        {
            await _createRandomData.CreateRandomFileWithRandomData(_settings.NumberOfRecordsLightLoad, stoppingToken),
            await _createRandomData.CreateRandomFileWithRandomData(_settings.NumberOfRecordsMediumLoad, stoppingToken),
            await _createRandomData.CreateRandomFileWithRandomData(_settings.NumberOfRecordsHeavyLoad, stoppingToken),
        };
        
        if (stoppingToken.IsCancellationRequested)
            return;

        _logger.LogInformation("files with random data are created");
        
        // Тест операций загрузки данных из файла в бд.
        foreach (var filePath in filePaths)
        {
            _withThread.Test(filePath);
            _withThreadPool.Test(filePath);
            _withTask.Test(filePath);
        }
        
        _hostApplicationLifetime.StopApplication();
    }
}