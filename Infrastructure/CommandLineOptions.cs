using CommandLine;

namespace dotnetcore.Infrastructure;

public class CommandLineOptions
{
    [Option('f', "file", Required = false, HelpText = "path to the file to fill with random data")]
    public string FileName { get; set; } = string.Empty;
    
    [Option('s', "size", Required = false, Default = 100, HelpText = "number of records to write to file")]
    public int NumberOfRecords { get; set; }
}