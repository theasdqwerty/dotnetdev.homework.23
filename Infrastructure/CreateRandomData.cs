using System.Collections.Concurrent;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using dotnetcore.Models;
using dotnetcore.Models.Entities;
using Faker;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace dotnetcore.Infrastructure;

public class CreateRandomData
{
    private readonly ILogger _logger;
    private readonly Settings _settings;
    public CreateRandomData(ILogger<CreateRandomData> logger, IOptions<Settings> options)
    {
        _logger = logger;
        _settings = options.Value;
    }

    /// <summary>
    /// Создать временный файл и заполнить его случайными данными, формата csv.
    /// </summary>
    public async Task<string> CreateRandomFileWithRandomData(int numberOfRecords, CancellationToken token = default)
        => await CreateFileWithRandomData(Path.GetTempFileName(), numberOfRecords, token);
    
    /// <summary>
    /// Создать файл и заполнить его случайными данными, формата csv.
    /// </summary>
    public async Task<string> CreateFileWithRandomData(string filePath, int numberOfRecords, 
        CancellationToken token = default)
    {
        var sw = Stopwatch.StartNew();
        
        // Создание рандомных записей о пользователях
        var partitioner = Partitioner.Create(0, numberOfRecords);
        var records = new Record[numberOfRecords];
        Parallel.ForEach(
            partitioner, 
            new ParallelOptions {MaxDegreeOfParallelism = _settings.NumberOfThreads},
            range =>
            {
                for (var i = range.Item1; i < range.Item2; i++)
                {
                    records[i] = new Record()
                    {
                        FullName = Name.FullName(),
                        Email = Internet.Email(),
                        PhoneNumber = Phone.Number()
                    };
                }
            });
        
        sw.Stop();

        await using var streamWriter = new StreamWriter(filePath);
        await using var csvWriter = new CsvWriter(streamWriter, 
            new CsvConfiguration(CultureInfo.InvariantCulture) { HasHeaderRecord = false });
        
        await csvWriter.WriteRecordsAsync(records, token);
        await streamWriter.FlushAsync();
        
        // 03.56 without specifying the number of threads
        // 02.27 count of threads 16
        // 02.10 count of threads is processor count
        
        _logger.LogInformation($"file {Path.GetFileName(filePath)} was created elapsed {sw.Elapsed:c}, number of records {numberOfRecords}");
        
        return filePath;
    }
}