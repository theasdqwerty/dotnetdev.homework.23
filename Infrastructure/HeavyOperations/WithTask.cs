using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using dotnetcore.Models;
using dotnetcore.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace dotnetcore.Infrastructure.HeavyOperations;

public class WithTask 
{
    private readonly Settings _settings;
    private readonly DbContextOptions<ApplicationDbContext> _dbContextOptions;

    public WithTask(IOptions<Settings> options, DbContextOptions<ApplicationDbContext> dbContextOptions)
    {
        _dbContextOptions = dbContextOptions;
        _settings = options.Value;
    }

    public void Test(string filePath)
    {
        using var streamReader = new StreamReader(filePath);
        using var csvReader = new CsvReader(streamReader,
            new CsvConfiguration(CultureInfo.InvariantCulture) {HasHeaderRecord = false});

        // Прочитаем данные файла
        var records = csvReader.GetRecords<Record>().ToList();
        var stopWatch = Stopwatch.StartNew();

        var tasks = new List<Task>();

        // Максимальное количество записей для одного потока
        var count = records.Count / _settings.NumberOfThreads;

        for (var index = 0; index < records.Count; index += count)
        {
            tasks.Add(Task.Factory.StartNew(state =>
            {
                if (state is List<Record> recordModels)
                {
                    var newContext = new ApplicationDbContext(_dbContextOptions);
                    newContext.Records.AddRange(recordModels);
                    newContext.SaveChanges();
                }
            }, records.Skip(index).Take(count).ToList()));
        }

        Task.WaitAll(tasks.ToArray());

        stopWatch.Stop();

        Console.WriteLine($"Operation with tasks, threads {_settings.NumberOfThreads} records {records.Count} elapsed time {stopWatch.Elapsed:c}");
    }
}