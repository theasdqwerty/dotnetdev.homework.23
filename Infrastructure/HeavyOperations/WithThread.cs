using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using CsvHelper;
using CsvHelper.Configuration;
using dotnetcore.Models;
using dotnetcore.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace dotnetcore.Infrastructure.HeavyOperations;

public class WithThread 
{
    private readonly Settings _settings;
    private readonly DbContextOptions<ApplicationDbContext> _dbContextOptions;
    public WithThread(IOptions<Settings> options, DbContextOptions<ApplicationDbContext> dbContextOptions)
    {
        _dbContextOptions = dbContextOptions;
        _settings = options.Value;
    }
    
    public void Test(string filePath)
    {
        using var streamReader = new StreamReader(filePath);
        using var csvReader = new CsvReader(streamReader, 
            new CsvConfiguration(CultureInfo.InvariantCulture) {HasHeaderRecord = false});
        
        // Прочитаем данные файла
        var records = csvReader.GetRecords<Record>().ToList();

        var stopWatch = Stopwatch.StartNew();
        
        // Максимальное количество записей для одного потока
        var count = records.Count / _settings.NumberOfThreads;
        var threads = new List<Thread>();

        for (var index = 0; index < records.Count; index += count)
        {
            var thread = new Thread(state =>
            {
                if (state is List<Record> recordModels)
                {
                    var newContext = new ApplicationDbContext(_dbContextOptions);
                    newContext.Records.AddRange(recordModels);
                    newContext.SaveChanges();
                }
            });
            thread.Start(records.Skip(index).Take(count).ToList());
            threads.Add(thread);
        }
        
        foreach (var thread in threads) 
            thread.Join();

        stopWatch.Stop();

        Console.WriteLine($"Operation with thread, threads {_settings.NumberOfThreads} records {records.Count} elapsed time {stopWatch.Elapsed:c}");
    }
}