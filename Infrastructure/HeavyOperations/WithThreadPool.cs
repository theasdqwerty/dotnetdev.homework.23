using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using CsvHelper;
using CsvHelper.Configuration;
using dotnetcore.Models;
using dotnetcore.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace dotnetcore.Infrastructure.HeavyOperations;

public class WithThreadPool 
{
    private readonly Settings _settings;
    private readonly DbContextOptions<ApplicationDbContext> _dbContextOptions;
    
    public WithThreadPool(IOptions<Settings> options, DbContextOptions<ApplicationDbContext> dbContextOptions)
    {
        _dbContextOptions = dbContextOptions;
        _settings = options.Value;
    }

    public void Test(string filePath)
    {
        using var streamReader = new StreamReader(filePath);
        using var csvReader = new CsvReader(streamReader, 
            new CsvConfiguration(CultureInfo.InvariantCulture) {HasHeaderRecord = false});

        // Прочитаем данные файла
        var records = csvReader.GetRecords<Record>().ToList();

        var stopWatch = Stopwatch.StartNew();
        
        // Максимальное количество записей для одного потока
        var count = records.Count / _settings.NumberOfThreads;

        var events = new List<ManualResetEvent>();

        for (var index = 0; index < records.Count; index += count)
        {
            var resetEvent = new ManualResetEvent(false);
            
            ThreadPool.QueueUserWorkItem(state =>
            {
                var newContext = new ApplicationDbContext(_dbContextOptions);
                newContext.Records.AddRange(state);
                newContext.SaveChangesAsync();
                resetEvent.Set();
            }, records.Skip(index).Take(count).ToList(), false);
            
            events.Add(resetEvent);
        }

        WaitHandle.WaitAll(events.Cast<WaitHandle>().ToArray());
        
        stopWatch.Stop();
        
        Console.WriteLine($"Operation with thread pool, threads {_settings.NumberOfThreads} records {records.Count} elapsed time {stopWatch.Elapsed:c}");
    }
}