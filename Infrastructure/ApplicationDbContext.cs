using dotnetcore.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace dotnetcore.Infrastructure;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions options) : base(options) { }

    public DbSet<Record> Records => Set<Record>();
}