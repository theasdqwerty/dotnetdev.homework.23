﻿using dotnetcore.Infrastructure;
using dotnetcore.Infrastructure.HeavyOperations;
using dotnetcore.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

var builder = Host.CreateDefaultBuilder(args)
    .ConfigureAppConfiguration(configuration => configuration.AddCommandLine(args))
    .ConfigureServices((context, services) =>
    {
        services.Configure<Settings>(context.Configuration.GetSection(Settings.SectionName));
        services.AddLogging(cfg => cfg.AddConsole());
        services.AddHostedService<Workflow>();
        services.AddTransient<CreateRandomData>();
        services.AddTransient<WithThread>();
        services.AddTransient<WithTask>();
        services.AddTransient<WithThreadPool>();
        services.AddDbContext<ApplicationDbContext>(opt => opt.UseInMemoryDatabase("homework"), 
            ServiceLifetime.Transient);
    });

var app = builder.Build();
app.Run();