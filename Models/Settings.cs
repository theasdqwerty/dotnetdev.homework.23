namespace dotnetcore.Models;

/// <summary>
/// Настройки
/// </summary>
public class Settings
{
    /// <summary>
    /// Название секции
    /// </summary>
    public const string SectionName = "Settings";
    
    /// <summary>
    /// Количество потоков
    /// </summary>
    public int NumberOfThreads { get; set; }
    
    public int NumberOfRecordsLightLoad { get; set; }
    
    public int NumberOfRecordsMediumLoad { get; set; }
    
    public int NumberOfRecordsHeavyLoad { get; set; }
}