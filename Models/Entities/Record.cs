using System.ComponentModel.DataAnnotations;

namespace dotnetcore.Models.Entities;

public class Record
{
    [Key]
    public int Id { get; set; }

    public string FullName { get; set; } = string.Empty;

    public string Email { get; set; } = string.Empty;

    public string PhoneNumber { get; set; } = string.Empty;

    public override string ToString()
        => $"{Id}\t{FullName}\t{Email}\t{PhoneNumber}";
}